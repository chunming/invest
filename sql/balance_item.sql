-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- 主机： localhost
-- 生成日期： 2022-06-23 15:06:41
-- 服务器版本： 5.7.26
-- PHP 版本： 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `invest`
--

-- --------------------------------------------------------

--
-- 表的结构 `balance_item`
--

CREATE TABLE `balance_item` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '项目名称',
  `company_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '公司代码',
  `parent_id` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `sort` smallint(5) UNSIGNED NOT NULL DEFAULT '100' COMMENT '排序',
  `display_bold` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT '报表显示是否加粗。1 不加粗 2 加粗'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='资产负债表列项说明';

--
-- 转存表中的数据 `balance_item`
--

INSERT INTO `balance_item` (`id`, `name`, `company_code`, `parent_id`, `sort`, `display_bold`) VALUES
(1, '资产', '000002', 0, 100, 2),
(2, '流动资产：', '000002', 1, 100, 2),
(3, '非流动资产：', '000002', 1, 101, 2),
(5, '货币资金', '000002', 2, 100, 1),
(6, '其他应收款', '000002', 2, 100, 1),
(7, '其他流动资产', '000002', 2, 100, 1),
(8, '流动资产合计', '000002', 1, 100, 2),
(9, '其他非流动资产', '000002', 3, 109, 1),
(10, '其他非流动金融资产', '000002', 3, 100, 1),
(12, '投资性房地产', '000002', 3, 100, 1),
(11, '长期股权投资', '000002', 3, 100, 1),
(13, '无形资产', '000002', 3, 100, 1),
(14, '固定资产', '000002', 3, 100, 1),
(15, '在建工程', '000002', 3, 100, 1),
(16, '非流动资产合计', '000002', 1, 110, 2),
(17, '资产总计', '000002', 1, 111, 2),
(18, '负债及股东权益', '000002', 0, 100, 2),
(19, '流动负债：', '000002', 18, 100, 2),
(20, '短期借款', '000002', 19, 100, 1),
(21, '应付票据', '000002', 19, 100, 1),
(22, '应付账款', '000002', 19, 100, 1),
(23, '合同负债', '000002', 19, 100, 1),
(24, '应付职工薪酬', '000002', 19, 100, 1),
(25, '应交税费', '000002', 19, 100, 1),
(26, '其他应付款', '000002', 19, 100, 1),
(27, '其他流动负债', '000002', 19, 100, 1),
(28, '一年内到期的非流动负债', '000002', 19, 100, 1),
(29, '流动负债合计', '000002', 18, 100, 2),
(30, '非流动负债：', '000002', 18, 100, 2),
(31, '长期借款', '000002', 30, 100, 1),
(32, '应付债券', '000002', 30, 100, 1),
(33, '非流动负债合计', '000002', 18, 100, 2),
(34, '负债合计', '000002', 18, 100, 2),
(35, '股东权益：', '000002', 18, 100, 2),
(36, '股本', '000002', 35, 100, 1),
(37, '资本公积', '000002', 35, 100, 1),
(38, '盈余公积', '000002', 35, 100, 1),
(39, '未分配利润', '000002', 35, 100, 1),
(40, '股东权益合计', '000002', 18, 100, 2),
(41, '负债及股东权益总计', '000002', 18, 100, 2),
(42, '交易性金融资产', '000002', 2, 100, 1),
(43, '资产', '000333', 0, 100, 2),
(44, '流动资产', '000333', 43, 100, 2),
(45, '货币资金', '000333', 44, 100, 1),
(46, '交易性金融资产', '000333', 44, 100, 1),
(47, '衍生金融资产', '000333', 44, 100, 1),
(48, '应收票据', '000333', 44, 100, 1),
(49, '应收账款', '000333', 44, 100, 1),
(50, '应收款项融资', '000333', 44, 100, 1),
(51, '预付款项', '000333', 44, 100, 1),
(52, '合同资产', '000333', 44, 100, 1),
(53, '发放贷款和垫款', '000333', 44, 100, 1),
(54, '其他应收款', '000333', 44, 100, 1),
(55, '存货', '000333', 44, 100, 1),
(56, '一年内到期的非流动资产', '000333', 44, 100, 1),
(57, '其他流动资产', '000333', 44, 100, 1),
(58, '流动资产合计', '000333', 43, 100, 1),
(59, '非流动资产', '000333', 43, 100, 2),
(60, '其他债权投资', '000333', 59, 100, 1),
(61, '长期应收款', '000333', 59, 100, 1),
(62, '发放贷款和垫款', '000333', 59, 100, 1),
(63, '长期股权投资', '000333', 59, 100, 1),
(64, '其他权益工具投资', '000333', 59, 100, 1),
(65, '其他非流动金融资产', '000333', 59, 100, 1),
(66, '投资性房地产', '000333', 59, 100, 1),
(67, '固定资产', '000333', 59, 100, 1),
(68, '在建工程', '000333', 59, 100, 1),
(69, '使用权资产', '000333', 59, 100, 1),
(70, '无形资产', '000333', 59, 100, 1),
(71, '商誉', '000333', 59, 100, 1),
(72, '长期待摊费用', '000333', 59, 100, 1),
(73, '递延所得税资产', '000333', 59, 100, 1),
(74, '其他非流动资产', '000333', 59, 100, 1),
(75, '非流动资产合计', '000333', 43, 100, 2),
(76, '资产总计', '000333', 0, 100, 2),
(77, '负债和股东权益', '000333', 0, 100, 2),
(78, '流动负债', '000333', 77, 100, 2),
(79, '短期借款', '000333', 78, 100, 1),
(80, '向中央银行借款', '000333', 78, 100, 1),
(81, '吸收存款及同业存放', '000333', 78, 100, 1),
(82, '衍生金融负债', '000333', 78, 100, 1),
(83, '应付票据', '000333', 78, 100, 1),
(84, '应付账款', '000333', 78, 100, 1),
(85, '合同负债', '000333', 78, 100, 1),
(86, '应付职工薪酬', '000333', 78, 100, 1),
(87, '应交税费', '000333', 78, 100, 1),
(88, '其他应付款', '000333', 78, 100, 1),
(89, '一年内到期的非流动负债', '000333', 78, 100, 1),
(90, '其他流动负债', '000333', 78, 100, 1),
(91, '流动负债合计', '000333', 77, 100, 2),
(92, '非流动负债', '000333', 77, 100, 2),
(93, '长期借款', '000333', 92, 100, 1),
(94, '租赁负债', '000333', 92, 100, 1),
(95, '长期应付款', '000333', 92, 100, 1),
(96, '预计负债', '000333', 92, 100, 1),
(97, '递延收益', '000333', 92, 100, 1),
(98, '长期应付职工薪酬', '000333', 92, 100, 1),
(99, '递延所得税负债', '000333', 92, 100, 1),
(100, '其他非流动负债', '000333', 92, 100, 1),
(101, '非流动负债合计', '000333', 77, 100, 2),
(102, '负债合计', '000333', 77, 100, 2),
(103, '股东权益', '000333', 77, 100, 2),
(104, '股本', '000333', 103, 100, 1),
(105, '资本公积', '000333', 103, 100, 1),
(106, '减：库存股', '000333', 103, 100, 1),
(107, '其他综合收益', '000333', 103, 100, 1),
(108, '一般风险准备', '000333', 103, 100, 1),
(109, '专项储备', '000333', 103, 100, 1),
(110, '盈余公积', '000333', 103, 100, 1),
(111, '未分配利润', '000333', 103, 100, 1),
(112, '归属于母公司股东权益合计', '000333', 103, 100, 1),
(113, '少数股东权益', '000333', 103, 100, 1),
(114, '股东权益合计', '000333', 103, 100, 2),
(115, '负债和股东权益总计', '000333', 77, 100, 2);

--
-- 转储表的索引
--

--
-- 表的索引 `balance_item`
--
ALTER TABLE `balance_item`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `balance_item`
--
ALTER TABLE `balance_item`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
