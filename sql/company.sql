-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- 主机： localhost
-- 生成日期： 2022-06-23 15:07:41
-- 服务器版本： 5.7.26
-- PHP 版本： 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `invest`
--

-- --------------------------------------------------------

--
-- 表的结构 `company`
--

CREATE TABLE `company` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '公司全称',
  `abbreviation` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '公司简称',
  `code` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '股票代码',
  `industry_id` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT '行业id',
  `stock_market_id` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT '证券市场id'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='公司基础';

--
-- 转存表中的数据 `company`
--

INSERT INTO `company` (`id`, `name`, `abbreviation`, `code`, `industry_id`, `stock_market_id`) VALUES
(1, '万科企业股份有限公司', '万科A', '000002', 4, 2),
(2, '中国平安保险(集团)股份有限公司', '中国平安', '601318', 3, 1),
(3, '美的集团股份有限公司', '美的集团', '000333', 1, 2);

--
-- 转储表的索引
--

--
-- 表的索引 `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `company`
--
ALTER TABLE `company`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
