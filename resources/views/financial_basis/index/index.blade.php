@extends('layouts.main')

@section('title', '财务基础')

@section('content')
<div class="row">
    <div class="col-md-2"><a href="{{ action('FinancialBasis\BalanceItemController@index') }}" class="btn btn-md btn-primary">资产负债表列项目</a></div>
</div>
@endsection