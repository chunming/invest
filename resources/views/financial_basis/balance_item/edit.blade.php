@extends('layouts.main')

@section('title', '资产负债表列项目编辑')

@section('content')
<ol class="breadcrumb">
    <li><a href="/">首页</a></li>
    <li><a href="{{ action('FinancialBasis\IndexController@index') }}">财务基础</a></li>
    <li><a href="{{ action('FinancialBasis\BalanceItemController@index') }}">资产负债表列项目</a></li>
    <li class="active">编辑</li>
  </ol>
<div class="btn-group" role="group">
    <a class="btn btn-primary" href="{{ action('FinancialBasis\BalanceItemController@index') }}">返回</a>
    <!-- <button type="button" class="btn btn-default">Middle</button>
    <button type="button" class="btn btn-default">Right</button> -->
</div>
<form class="form-horizontal" method="post" action="{{ action('FinancialBasis\BalanceItemController@update', $model) }}">
    @csrf
    @method('PUT')

    <div class="form-group">
        <label for="company_code" class="col-sm-2 control-label">公司</label>
        <div class="col-sm-4">
            <select class="form-control" id="company_code" name="company_code">
                @foreach($companies as $value)
                <option value="{{ $value->code }}" @if(old('company_code', $model->company_code) == $value->code) selected @endif>{{ $value->code.'---'.$value->name }}</option>
                @endforeach
            </select>
            @if($errors->first('company_code'))
			<p class="help-block error">{{ $errors->first('company_code') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">名称</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $model->name) }}">
            @if($errors->first('name'))
			<p class="help-block error">{{ $errors->first('name') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <label for="parent_id" class="col-sm-2 control-label">上级项目</label>
        <div class="col-sm-4">
            <select class="form-control" id="parent_id" name="parent_id">
                <option value="0">请选择上级项目</option>
                @foreach($balance_items as $value)
                <option value="{{ $value->id }}" @if(old('parent_id', $model->parent_id) == $value->id) selected @endif  @if($value->id == $model->id) disabled @endif>{{ str_repeat('---', $value->level * 2).$value->name }}</option>
                @endforeach
            </select>
            @if($errors->first('parent_id'))
			<p class="help-block error">{{ $errors->first('parent_id') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <label for="sort" class="col-sm-2 control-label">排序</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="sort" name="sort" value="{{ old('sort', $model->sort) }}">
            @if($errors->first('sort'))
			<p class="help-block error">{{ $errors->first('sort') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <label for="display_bold" class="col-sm-2 control-label">显示是否加粗</label>
        <div class="col-sm-4">
            <select class="form-control" id="display_bold" name="display_bold">
                @foreach(config('basis.display_bold') as $key => $value)
                <option value="{{ $key }}" @if(old('display_bold') == $key || $model->display_bold == $key) selected @endif>{{ $value }}</option>
                @endforeach
            </select>
            @if($errors->first('display_bold'))
			<p class="help-block error">{{ $errors->first('display_bold') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">提交</button>
        </div>
    </div>
</form>

<script>
    $(document).ready(function(){
        //当选择公司时，列项上级会更新
        $("#company_code").change(function(){
            var list = $('#parent_id');
            list.find("option").remove();
            var company_code = $(this).val();
            request_balance_item(company_code, list);
        });
    });
</script>
@endsection