@extends('layouts.main')

@section('title', '资产负债表列项目显示')

@section('content')
<ol class="breadcrumb">
    <li><a href="/">首页</a></li>
    <li><a href="{{ action('FinancialBasis\IndexController@index') }}">财务基础</a></li>
    <li><a href="{{ action('FinancialBasis\BalanceItemController@index') }}">资产负债表列项目</a></li>
    <li class="active">显示</li>
  </ol>
<div class="btn-group" role="group">
    <a class="btn btn-primary" href="{{ action('FinancialBasis\BalanceItemController@index') }}">返回</a>
    <!-- <button type="button" class="btn btn-default">Middle</button>
    <button type="button" class="btn btn-default">Right</button> -->
</div>

@include('common.alert')

<form class="form-horizontal">

    <div class="form-group">
        <label for="company_code" class="col-sm-2 control-label">公司</label>
        <div class="col-sm-4">
            <select class="form-control" id="company_code" name="company_code" disabled>
                <option value="{{ $model->company_code }}">{{ $model->company_code.'---'.$model->company->name }}</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">名称</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="name" name="name" value="{{ $model->name }}" disabled>
        </div>
    </div>

    <div class="form-group">
        <label for="parent_id" class="col-sm-2 control-label">上级项目</label>
        <div class="col-sm-4">
            <select class="form-control" id="parent_id" name="parent_id" disabled>
                @if($model->parent_id == 0)
                <option value="0">无上级项目</option>
                @else
                <option value="{{ $model->parent_id }}">{{ $model->parent->name }}</option>
                @endif
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="sort" class="col-sm-2 control-label">排序</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="sort" name="sort" value="{{ $model->sort }}" disabled>
        </div>
    </div>

    <div class="form-group">
        <label for="display_bold" class="col-sm-2 control-label">显示是否加粗</label>
        <div class="col-sm-4">
            <select class="form-control" id="display_bold" name="display_bold" disabled>
                @foreach(config('basis.display_bold') as $key => $value)
                <option value="{{ $key }}" @if($model->display_bold == $key) selected @endif>{{ $value }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <a href="{{ action('FinancialBasis\BalanceItemController@index', ['company_code' => $model->company_code]) }}" class="btn btn-primary">返回</a>
            <a href="{{ action('FinancialBasis\BalanceItemController@create', ['company_code' => $model->company_code, 'parent_id' => $model->parent_id]) }}" class="btn btn-primary">继续添加</a>
            <a href="{{ action('FinancialBasis\BalanceItemController@edit', $model) }}" class="btn btn-warning">编辑</a>
        </div>
    </div>
</form>
@endsection