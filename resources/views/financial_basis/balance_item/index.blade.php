@extends('layouts.main')

@section('title', '资产负债表列项目')

@section('content')
<ol class="breadcrumb">
    <li><a href="/">首页</a></li>
    <li><a href="{{ action('FinancialBasis\IndexController@index') }}">财务基础</a></li>
    <li class="active">资产负债表列项目</li>
  </ol>
<div class="btn-group" role="group">
    <a class="btn btn-primary" href="{{ action('FinancialBasis\BalanceItemController@create') }}">添加</a>
    <!-- <button type="button" class="btn btn-default">Middle</button>
    <button type="button" class="btn btn-default">Right</button> -->
</div>
<div class="clearfix"></div>
<hr>

<form class="form-inline pull-right" method="get" action="{{ action('FinancialBasis\BalanceItemController@index') }}">
    
    <div class="form-group">
        <label for="company_code">公司</label>
        <select class="form-control" id="company_code" name="company_code">
            <option value="0">请选择公司</option>
            @foreach($companies as $value)
            <option value="{{ $value->code }}" @if(request()->company_code == $value->code) selected @endif>{{ $value->code.'--'.$value->name }}</option>
            @endforeach
          </select>
    </div>

    <button type="submit" name="search" value="search" class="btn btn-primary">查询</button>
    <!-- <button type="submit" name="delete" value="delete" class="btn btn-danger">根据条件删除数据</button> -->

</form>

<div class="clearfix"></div>
<hr>

<table class="table table-striped table-bordered table-hover table-condensed">
    <thead>
    </thead>
    <tbody>
        <tr>
            <th>序列号</th>
            <th>列项目名称</th>
            <th>排序</th>
            <th>操作</th>
        </tr>
        @forelse($models as $model)
        <tr>
            <td>{{ $model->id }}</td>
            <td><a @if($model->display_bold == 2) class="bold-big" @endif href="{{ action('FinancialBasis\BalanceItemController@show', $model) }}" >{{ str_repeat('---', $model->level * 2).$model->name }}</a></td>
            <td>{{ $model->sort }}</td>
            <td>
                <a class="btn btn-warning btn-sm" href="{{ action('FinancialBasis\BalanceItemController@edit', $model) }}">编辑</a>
                <button type="button" class="btn btn-danger btn-sm" name="delete" value="{{ $model->id }}">删除</button>
            </td>
        </tr>
        @empty
        <tr><td>无数据</td></tr>
        @endforelse
    </tbody>
   
</table>

<script>
    $(document).ready(function(){
        $("button[name=delete]").click(function(){
            var that = $(this);
            var id = that.attr("value");
            $.post("/financial_basis/balance_item/" + id, {
                '_method': 'DELETE',
                '_token': '{{ csrf_token() }}',
            }, function(data){
                switch(data.status){
                    case 0:
                        that.parent().parent('tr').remove();
                        alert("删除信息成功！");
                        break;
                    case 1:
                        alert(data.info);
                        break;
                }
            });
        });
    });

</script>
@endsection