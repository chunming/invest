@extends('layouts.main')

@section('title', '资产负债表列项目添加')

@section('content')
<ol class="breadcrumb">
    <li><a href="/">首页</a></li>
    <li><a href="{{ action('FinancialBasis\IndexController@index') }}">财务基础</a></li>
    <li><a href="{{ action('FinancialBasis\BalanceItemController@index') }}">资产负债表列项目</a></li>
    <li class="active">添加</li>
  </ol>
<div class="btn-group" role="group">
    <a class="btn btn-primary" href="{{ action('FinancialBasis\BalanceItemController@index') }}">返回</a>
    <!-- <button type="button" class="btn btn-default">Middle</button>
    <button type="button" class="btn btn-default">Right</button> -->
</div>

@include('common.alert')

<form class="form-horizontal" method="post" action="{{ action('FinancialBasis\BalanceItemController@store') }}">
    @csrf

    <div class="form-group">
        <label for="company_code" class="col-sm-2 control-label">公司</label>
        <div class="col-sm-4">
            <select class="form-control" id="company_code" name="company_code">
                <option value="0">请选择公司</option>
                @foreach($companies as $value)
                <option value="{{ $value->code }}" @if(old('company_code', request()->company_code) == $value->code) selected @endif>{{ $value->code.'---'.$value->name }}</option>
                @endforeach
            </select>
            @if($errors->first('company_code'))
			<p class="help-block error">{{ $errors->first('company_code') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">列项名称</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
            @if($errors->first('name'))
			<p class="help-block error">{{ $errors->first('name') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <label for="parent_id" class="col-sm-2 control-label">上级项目</label>
        <div class="col-sm-4">
            <select class="form-control" id="parent_id" name="parent_id">
            </select>
            @if($errors->first('parent_id'))
			<p class="help-block error">{{ $errors->first('parent_id') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <label for="sort" class="col-sm-2 control-label">排序</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="sort" name="sort" value="{{ old('sort', 100)}}">
            @if($errors->first('sort'))
			<p class="help-block error">{{ $errors->first('sort') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <label for="display_bold" class="col-sm-2 control-label">显示是否加粗</label>
        <div class="col-sm-4">
            <select class="form-control" id="display_bold" name="display_bold">
                @foreach(config('basis.display_bold') as $key => $value)
                <option value="{{ $key }}" @if(old('display_bold') == $key) selected @endif>{{ $value }}</option>
                @endforeach
            </select>
            @if($errors->first('display_bold'))
			<p class="help-block error">{{ $errors->first('display_bold') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">提交</button>
        </div>
    </div>
</form>

<script>
    $(document).ready(function(){
        var company_code = $('#company_code').val();
        var list = $('#parent_id');
        var parent_id = getUrlParams('parent_id');
        request_balance_item(company_code, list, parent_id);
        //当选择公司时，列项上级会更新
        $("#company_code").change(function(){
            list.find("option").remove();
            var company_code = $(this).val();
            request_balance_item(company_code, list);
        });
    });

</script>
@endsection


