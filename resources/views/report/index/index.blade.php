@extends('layouts.main')

@section('title', '报表')

@section('content')
<div class="row">
    <div class="col-md-2"><a href="{{ action('Report\BalanceController@tabular') }}" class="btn btn-md btn-primary">资产负债表(表格)</a></div>
    <div class="col-md-2"><a href="{{ action('Report\BalanceController@visual') }}" class="btn btn-md btn-primary">资产负债表(可视化)</a></div>
</div>
@endsection