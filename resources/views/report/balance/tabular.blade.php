@extends('layouts.main')

@section('title', '资产负债表')

@section('content')
<ol class="breadcrumb">
    @include('report.balance.breadcrumb')
    <li class="active">资产负债表(表格)</li>
</ol>
<div class="btn-group" role="group">
    <a class="btn btn-primary" href="{{ action('Company\BalanceSheetController@create') }}">添加</a>
    <a class="btn btn-primary" href="{{ action('Report\BalanceController@visual') }}">可视化</a>
</div>
<div class="clearfix"></div>
<hr>

<form class="form-inline pull-right" method="get" action="{{ action('Report\BalanceController@tabular') }}">
    
    <div class="form-group">
        <label for="company_code">公司</label>
        <select class="form-control" id="company_code" name="company_code">
            <option value="0">请选择公司</option>
            @foreach($companies as $value)
            <option value="{{ $value->code }}" @if(request()->company_code == $value->code || $balance_list->company_code == $value->code) selected @endif>{{ $value->code.'--'.$value->name }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="year">年份</label>
        <input type='text' class="form-control" style="width:60px" id='year_start' name="year_start" value="{{ request()->year_start ? request()->year_start : $year_start }}" />
        ---
        <input type='text' class="form-control" style="width:60px" id='year_end' name="year_end" value="{{ request()->year_end ? request()->year_end : $year_end}}" />
    </div>

    <div class="form-group">
        <label for="month">财报类型</label>
        <select class="form-control" id="report_category_id" name="report_category_id">
            <option>请选择财报类型</option>
            @foreach($financial_report_category as $value)
            <option value="{{ $value->id }}" @if(request()->report_category_id == $value->id || $balance_list->report_category_id == $value->id) selected @endif>{{ $value->name }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="month">财报范围</label>
        <select class="form-control" id="report_range_id" name="report_range_id">
            <option>请选择财报范围</option>
            @foreach($financial_report_range as $value)
            <option value="{{ $value->id }}" @if(request()->report_range_id == $value->id || $balance_list->report_range_id == $value->id) selected @endif>{{ $value->name }}</option>
            @endforeach
        </select>
    </div>

    <button type="submit" name="search" value="search" class="btn btn-primary">查询</button>
    <!-- <button type="submit" name="delete" value="delete" class="btn btn-danger">根据条件删除数据</button> -->

</form>
  
<div class="clearfix"></div>
<hr>
<span>编制单位： {{ $balance_list->company()->name }}</span>
<!-- <span></span> -->
<!-- <span>币种: 人民币</span> -->
<table class="table table-striped table-bordered table-hover table-condensed">
    <tbody>
        <tr>
            <th>列项目</th>
            @for($i = $year_end; $i >= $year_start; $i--)
            <th>{{ $i . '年'}}</th>
            @endfor
        </tr>
        @foreach($balance_list->balance_items as $balance_item)
        <tr @if($balance_item->display_bold == 2) class="bold-big" @endif>
            <td>{{ str_repeat('---', $balance_item->level).$balance_item->name }}</td>
            @for($i = $year_end; $i >= $year_start; $i--)
            <td>{{ $balance_list->getItem($i, $balance_item->id) }}</td>
            @endfor
        </tr>
        @endforeach
    </tbody>
   
</table>

@endsection