@extends('layouts.main')

@section('title', '资产负债表明细')

@section('content')
<ol class="breadcrumb">
    @include('company.balance_sheet.breadcrumb')
    <li class="active">资产负债表明细</li>
  </ol>
<div class="btn-group" role="group">
    <a class="btn btn-primary" href="{{ action('Company\BalanceSheetController@create') }}">添加</a>
</div>
<div class="clearfix"></div>
<hr>

<form class="form-inline pull-right" method="get" action="{{ action('Company\BalanceSheetController@index') }}">
    
    <div class="form-group">
        <label for="company_code">公司</label>
        <select class="form-control" id="company_code" name="company_code">
            <option value="0">请选择公司</option>
            @foreach($companies as $value)
            <option value="{{ $value->code }}" @if(request()->company_code == $value->code) selected @endif>{{ $value->code.'--'.$value->name }}</option>
            @endforeach
          </select>
    </div>

    <div class="form-group">
        <label for="year">年份</label>
        <input type='text' class="form-control" id='year' name="year" value="{{ request()->year }}" />
    </div>

    <div class="form-group">
        <label for="month">财报类型</label>
        <select class="form-control" id="financial_report_category" name="financial_report_category">
            <option>请选择财报类型</option>
            @foreach($financial_report_category as $value)
            <option value="{{ $value->id }}" @if(request()->financial_report_category == $value->id) selected @endif>{{ $value->name }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="month">财报范围</label>
        <select class="form-control" id="financial_report_range" name="financial_report_range">
            <option>请选择财报范围</option>
            @foreach($financial_report_range as $value)
            <option value="{{ $value->id }}" @if(request()->financial_report_range == $value->id) selected @endif>{{ $value->name }}</option>
            @endforeach
        </select>
    </div>

    <button type="submit" name="search" value="search" class="btn btn-primary">查询</button>
    <!-- <button type="submit" name="delete" value="delete" class="btn btn-danger">根据条件删除数据</button> -->

</form>

<div class="clearfix"></div>
<hr>

<table class="table table-striped table-bordered table-hover table-condensed">
    <tbody>
        <tr>
            <th>序列号</th>
            <th>公司简称</th>
            <th>年份</th>
            <th>财报类型</th>
            <th>财报范围</th>
            <th>列项目</th>
            <th>金额</th>
            <th>操作</th>
        </tr>
        @foreach($models as $model)
        <tr>
            <td>{{ $model->id }}</td>
            <td>{{ $model->company->abbreviation }}</td>
            <td>{{ $model->year }}</td>
            <td>{{ $model->financial_report_category->name }}</td>
            <td>{{ $model->financial_report_range->name }}</td>
            <td>{{ $model->balance_item->name }}</td>
            <td>{{ $model->amount }}</td>
            <td>
                <a class="btn btn-warning btn-sm" href="{{ action('Company\BalanceSheetController@edit', $model) }}">编辑</a>
                <a class="btn btn-warning btn-sm" href="{{ action('Company\BalanceSheetController@show', $model) }}">查看</a>
                <button type="button" class="btn btn-danger btn-sm" name="delete" value="{{ $model->id }}">删除</button>
            </td>
        </tr>
        @endforeach
    </tbody>
   
</table>
{{ $models->appends([
    'company_code' => request()->company_code,
    'year' => request()->year,
    'financial_report_category' => request()->financial_report_category,
    'financial_report_range' => request()->financial_report_range,
    ])->links() }}
<script>
    $(document).ready(function(){
        $("button[name=delete]").click(function(){
            var that = $(this);
            var id = that.attr("value");
            $.post("/company/balance_sheet/" + id, {
                '_method': 'DELETE',
                '_token': '{{ csrf_token() }}',
            }, function(data){
                switch(data.status){
                    case 0:
                        that.parent().parent('tr').remove();
                        alert("删除信息成功！");
                        break;
                    case 1:
                        alert(data.info);
                        break;
                }
            });
        });
    });

</script>
@endsection