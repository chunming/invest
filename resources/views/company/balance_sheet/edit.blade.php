@extends('layouts.main')

@section('title', '编辑资产负债表明细')

@section('content')
<ol class="breadcrumb">
    @include('company.balance_sheet.breadcrumb')
    <li class="active">编辑</li>
</ol>
<div class="btn-group" role="group">
    <a class="btn btn-primary" href="{{ action('Company\BalanceSheetController@index') }}">返回</a>
</div>
<form class="form-horizontal" method="post" action="{{ action('Company\BalanceSheetController@update', $model) }}">
    @csrf
    @method('PUT')

    <div class="form-group">
        <label for="company_code" class="col-sm-2 control-label">公司</label>
        <div class="col-sm-4">
            <select class="form-control" id="company_code" name="company_code">
                @foreach($companies as $value)
                <option value="{{ $value->code }}" @if(old('company_code', $model->company_code) == $value->code ) selected @endif>{{ $value->code.'--'.$value->name }}</option>
                @endforeach
            </select>
            @if($errors->first('company_code'))
			<p class="help-block error">{{ $errors->first('company_code') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <label for="year" class="col-sm-2 control-label">年份</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="year" name="year" value="{{ old('year', $model->year) }}">
            @if($errors->first('year'))
			<p class="help-block error">{{ $errors->first('year') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <label for="report_category_id" class="col-sm-2 control-label">财报类型</label>
        <div class="col-sm-4">
            <select class="form-control" id="report_category_id" name="report_category_id">
                @foreach($financial_report_category as $value)
                <option value="{{ $value->id }}" @if(old('report_category_id', $model->report_category_id) == $value->id) selected @endif>{{ $value->name }}</option>
                @endforeach
            </select>
            @if($errors->first('report_category_id'))
			<p class="help-block error">{{ $errors->first('report_category_id') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <label for="report_range_id" class="col-sm-2 control-label">财报范围</label>
        <div class="col-sm-4">
            <select class="form-control" id="report_range_id" name="report_range_id">
                @foreach($financial_report_range as $value)
                <option value="{{ $value->id }}" @if(old('report_range_id', $model->report_range_id) == $value->name) selected @endif>{{ $value->name }}</option>
                @endforeach
            </select>
            @if($errors->first('report_range_id'))
			<p class="help-block error">{{ $errors->first('report_range_id') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <label for="balance_item_id" class="col-sm-2 control-label">资产负债表列项</label>
        <div class="col-sm-4">
            <select class="form-control" id="balance_item_id" name="balance_item_id">
                @foreach($balance_items as $value)
                <option @if($value->parent_id == 0) disabled @endif @if($value->display_bold == 2) class="bold-big" @endif value="{{ $value->id }}" @if(old('balance_item_id', $model->balance_item_id) == $value->id) selected @endif>{{ str_repeat('---', $value->level * 2).$value->name }}</option>
                @endforeach
            </select>
            @if($errors->first('balance_item_id'))
			<p class="help-block error">{{ $errors->first('balance_item_id') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <label for="multiple" class="col-sm-2 control-label">单位</label>
        <div class="col-sm-4">
            <select class="form-control" id="multiple" name="multiple">
                <option value="0">请选择单位</option>
                @foreach($units as $value)
                <option value="{{ $value->multiple }}" @if($value->multiple == request()->cookie('multiple')) selected @endif>{{ $value->name }}</option>
                @endforeach
            </select>
            @if($errors->first('multiple'))
			<p class="help-block error">{{ $errors->first('multiple') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <label for="amount" class="col-sm-2 control-label">金额(元)</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="amount" name="amount" value="{{ old('amount', $model->amount) }}">
            @if($errors->first('amount'))
			<p class="help-block error">{{ $errors->first('amount') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">提交</button>
        </div>
    </div>
</form>
@endsection