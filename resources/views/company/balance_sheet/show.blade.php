@extends('layouts.main')

@section('title', '显示资产负债表明细')

@section('content')
<ol class="breadcrumb">
    @include('company.balance_sheet.breadcrumb')
    <li class="active">显示</li>
</ol>
<div class="btn-group" role="group">
    <a class="btn btn-primary" href="{{ action('Company\BalanceSheetController@index') }}">返回</a>
</div>

@include('common.alert')

<form class="form-horizontal">

    <div class="form-group">
        <label for="company_code" class="col-sm-2 control-label">公司</label>
        <div class="col-sm-4">
            <select class="form-control" id="company_code" name="company_code" disabled>
                <option>{{ $model->company->code.'---'.$model->company->name }}</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="year" class="col-sm-2 control-label">年份</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="year" name="year" value="{{ $model->year }}" disabled>
        </div>
    </div>

    <div class="form-group">
        <label for="report_category_id" class="col-sm-2 control-label">财报类型</label>
        <div class="col-sm-4">
            <select class="form-control" id="report_category_id" name="report_category_id" disabled>
                <option value="{{ $model->report_category_id }}">{{ $model->financial_report_category->name }}</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="report_range_id" class="col-sm-2 control-label">财报范围</label>
        <div class="col-sm-4">
            <select class="form-control" id="report_range_id" name="report_range_id" disabled>
                <option value="{{ $model->report_range_id }}">{{ $model->financial_report_range->name }}</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="balance_item_id" class="col-sm-2 control-label">资产负债表列项</label>
        <div class="col-sm-4">
            <select class="form-control" id="balance_item_id" name="balance_item_id" disabled>
                <option value="{{ $model->balance_item_id }}">{{ $model->balance_item->name }}</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="amount" class="col-sm-2 control-label">金额(元)</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="amount" name="amount" value="{{ $model->amount }}" disabled>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <a href="{{ action('Company\BalanceSheetController@create', ['company_code' => $model->company_code, 'year' => $model->year, 'report_category_id' => $model->report_category_id, 'report_range_id' => $model->report_range_id]) }}" class="btn btn-primary">继续添加</a>
            <a href="{{ action('Company\BalanceSheetController@edit', $model) }}" class="btn btn-warning">编辑</a>
            <button type="button" class="btn btn-danger" name="delete" value="{{ $model->id }}">删除</button>
        </div>
    </div>
</form>

<script>
    $(document).ready(function(){
        $("button[name=delete]").click(function(){
            var that = $(this);
            var id = that.attr("value");
            $.post("/company/balance_sheet/" + id, {
                '_method': 'DELETE',
                '_token': '{{ csrf_token() }}',
            }, function(data){
                switch(data.status){
                    case 0:
                        alert("删除信息成功！");
                        window.location.href = '/company/balance_sheet';
                        break;
                    case 1:
                        alert(data.info);
                        break;
                }
            });
        });
    });

</script>
@endsection

