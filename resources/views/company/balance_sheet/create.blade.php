@extends('layouts.main')

@section('title', '添加资产负债表明细')

@section('content')
<ol class="breadcrumb">
    @include('company.balance_sheet.breadcrumb')
    <li class="active">添加</li>
</ol>
<div class="btn-group" role="group">
    <a class="btn btn-primary" href="{{ action('Company\BalanceSheetController@index') }}">返回</a>
</div>

@include('common.alert')

<form class="form-horizontal" method="post" action="{{ action('Company\BalanceSheetController@store') }}">
    @csrf

    <div class="form-group">
        <label for="company_code" class="col-sm-2 control-label">公司</label>
        <div class="col-sm-4">
            <select class="form-control" id="company_code" name="company_code">
                <option>请选择公司</option>
                @foreach($companies as $value)
                <option value="{{ $value->code }}" @if(old('company_code', request()->company_code) == $value->code ) selected @endif>{{ $value->code.'--'.$value->name }}</option>
                @endforeach
            </select>
            @if($errors->first('company_code'))
			<p class="help-block error">{{ $errors->first('company_code') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <label for="year" class="col-sm-2 control-label">年份</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="year" name="year" value="{{ old('year', request()->year) }}">
            @if($errors->first('year'))
			<p class="help-block error">{{ $errors->first('year') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <label for="report_category_id" class="col-sm-2 control-label">财报类型</label>
        <div class="col-sm-4">
            <select class="form-control" id="report_category_id" name="report_category_id">
                <option value="0">请选择财报类型</option>
                @foreach($financial_report_category as $value)
                <option value="{{ $value->id }}" @if(old('report_category_id', request()->report_category_id) == $value->id) selected @endif>{{ $value->name }}</option>
                @endforeach
            </select>
            @if($errors->first('report_category_id'))
			<p class="help-block error">{{ $errors->first('report_category_id') }}</p>
			@endif
        </div>
    </div>
    
    <div class="form-group">
        <label for="report_range_id" class="col-sm-2 control-label">财报范围</label>
        <div class="col-sm-4">
            <select class="form-control" id="report_range_id" name="report_range_id">
                <option value="0">请选择财报范围</option>
                @foreach($financial_report_range as $value)
                <option value="{{ $value->id }}" @if(old('report_range_id', request()->report_range_id) == $value->id) selected @endif>{{ $value->name }}</option>
                @endforeach
            </select>
            @if($errors->first('report_range_id'))
			<p class="help-block error">{{ $errors->first('report_range_id') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <label for="balance_item_id" class="col-sm-2 control-label">资产负债表列项</label>
        <div class="col-sm-4">
            <select class="form-control" id="balance_item_id" name="balance_item_id">
                <option value="0">请选择资产负债表列项</option>
            </select>
            @if($errors->first('balance_item_id'))
			<p class="help-block error">{{ $errors->first('balance_item_id') }}</p>
			@endif
        </div>
    </div>
    
    <div class="form-group">
        <label for="multiple" class="col-sm-2 control-label">单位</label>
        <div class="col-sm-4">
            <select class="form-control" id="multiple" name="multiple">
                <option value="0">请选择单位</option>
                @foreach($units as $value)
                <option value="{{ $value->multiple }}" @if($value->multiple == request()->cookie('multiple')) selected @endif>{{ $value->name }}</option>
                @endforeach
            </select>
            @if($errors->first('multiple'))
			<p class="help-block error">{{ $errors->first('multiple') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <label for="amount" class="col-sm-2 control-label">金额(元)</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="amount" name="amount" value="{{ old('amount') }}">
            @if($errors->first('amount'))
			<p class="help-block error">{{ $errors->first('amount') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">提交</button>
        </div>
    </div>
</form>

<script>
    $(document).ready(function(){
        var company_code = $('#company_code').val();
        var list = $('#balance_item_id');
        // var parent_id = getUrlParams('parent_id');
        request_balance_item(company_code, list);
        //当选择公司时，列项上级会更新
        $("#company_code").change(function(){
            list.find("option").remove();
            var company_code = $(this).val();
            request_balance_item(company_code, list);
        });
    });

</script>
@endsection