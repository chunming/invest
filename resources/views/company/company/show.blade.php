@extends('layouts.main')

@section('title', '公司显示')

@section('content')
<ol class="breadcrumb">
    @include('company.company.breadcrumb')
    <li class="active">显示</li>
</ol>
<div class="btn-group" role="group">
    <a class="btn btn-primary" href="{{ action('Company\CompanyController@index') }}">返回</a>
</div>

@include('common.alert')

<form class="form-horizontal">

    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">公司全称</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="name" name="name" value="{{ $model->name }}" disabled>
        </div>
    </div>

    <div class="form-group">
        <label for="abbreviation" class="col-sm-2 control-label">公司简称</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="abbreviation" name="abbreviation" value="{{ $model->abbreviation }}" disabled>
        </div>
    </div>

    <div class="form-group">
        <label for="code" class="col-sm-2 control-label">股票代码</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="code" name="code" value="{{ $model->code }}" disabled>
        </div>
    </div>

    <div class="form-group">
        <label for="industry_id" class="col-sm-2 control-label">所属行业</label>
        <div class="col-sm-4">
            <select class="form-control" id="industry_id" name="industry_id" disabled>
                <option value="{{ $model->industry_id }}">{{ $model->industry->name }}</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="stock_market_id" class="col-sm-2 control-label">证券市场</label>
        <div class="col-sm-4">
            <select class="form-control" id="stock_market_id" name="stock_market_id" disabled>
                <option value="{{ $model->stock_market_id }}">{{ $model->stock_market ? $model->stock_market->name : '' }}</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <a href="{{ action('Company\CompanyController@index') }}" class="btn btn-primary">返回</a>
            <a href="{{ action('Company\CompanyController@create') }}" class="btn btn-primary">添加</a>
            <a href="{{ action('Company\CompanyController@edit', $model) }}" class="btn btn-warning">编辑</a>
        </div>
    </div>
</form>
@endsection