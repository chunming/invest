@extends('layouts.main')

@section('title', '公司编辑')

@section('content')
<ol class="breadcrumb">
    @include('company.company.breadcrumb')
    <li class="active">编辑</li>
</ol>
<div class="btn-group" role="group">
    <a class="btn btn-primary" href="{{ action('Company\CompanyController@index') }}">返回</a>
</div>
<form class="form-horizontal" method="post" action="{{ action('Company\CompanyController@update', $model) }}">
    @csrf
    @method('PUT')

    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">名称</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $model->name) }}">
            @if($errors->first('name'))
			<p class="help-block error">{{ $errors->first('name') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <label for="abbreviation" class="col-sm-2 control-label">公司简称</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="abbreviation" name="abbreviation" value="{{ old('abbreviation', $model->abbreviation) }}">
            @if($errors->first('abbreviation'))
			<p class="help-block error">{{ $errors->first('abbreviation') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <label for="code" class="col-sm-2 control-label">股票代码</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="code" name="code" value="{{ old('code', $model->code) }}">
            @if($errors->first('code'))
			<p class="help-block error">{{ $errors->first('code') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <label for="industry_id" class="col-sm-2 control-label">行业</label>
        <div class="col-sm-4">
            <select class="form-control" id="industry_id" name="industry_id">
                @foreach($industries as $value)
                <option value="{{ $value->id }}" @if(old('industry_id') == $value->id || $model->industry_id == $value->id) selected @endif>{{ $value->name }}</option>
                @endforeach
            </select>
            @if($errors->first('industry_id'))
			<p class="help-block error">{{ $errors->first('industry_id') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <label for="stock_market_id" class="col-sm-2 control-label">证券市场</label>
        <div class="col-sm-4">
            <select class="form-control" id="stock_market_id" name="stock_market_id">
                @foreach($stock_markets as $value)
                <option value="{{ $value->id }}" @if(old('stock_market_id', $model->stock_market_id) == $value->id) selected @endif>{{ $value->name }}</option>
                @endforeach
            </select>
            @if($errors->first('stock_market_id'))
			<p class="help-block error">{{ $errors->first('stock_market_id') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">提交</button>
        </div>
    </div>
</form>
@endsection