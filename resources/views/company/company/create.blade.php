@extends('layouts.main')

@section('title', '公司添加')

@section('content')
<ol class="breadcrumb">
    @include('company.company.breadcrumb')
    <li class="active">添加</li>
</ol>
<div class="btn-group" role="group">
    <a class="btn btn-primary" href="{{ action('Company\CompanyController@index') }}">返回</a>
</div>
<form class="form-horizontal" method="post" action="{{ action('Company\CompanyController@store') }}">
    @csrf

    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">公司全称</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
            @if($errors->first('name'))
			<p class="help-block error">{{ $errors->first('name') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <label for="abbreviation" class="col-sm-2 control-label">公司简称</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="abbreviation" name="abbreviation" value="{{ old('abbreviation') }}">
            @if($errors->first('abbreviation'))
			<p class="help-block error">{{ $errors->first('abbreviation') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <label for="code" class="col-sm-2 control-label">股票代码</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="code" name="code" value="{{ old('code') }}">
            @if($errors->first('code'))
			<p class="help-block error">{{ $errors->first('code') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <label for="industry_id" class="col-sm-2 control-label">行业</label>
        <div class="col-sm-4">
            <select class="form-control" id="industry_id" name="industry_id">
                <option value="0">请选择行业</option>
                @foreach($industries as $value)
                <option value="{{ $value->id }}" @if(old('industry_id') == $value->id) selected @endif>{{ $value->name }}</option>
                @endforeach
            </select>
            @if($errors->first('industry_id'))
			<p class="help-block error">{{ $errors->first('industry_id') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <label for="stock_market_id" class="col-sm-2 control-label">证券市场</label>
        <div class="col-sm-4">
            <select class="form-control" id="stock_market_id" name="stock_market_id">
                <option value="0">请选择证券市场</option>
                @foreach($stock_markets as $value)
                <option value="{{ $value->id }}" @if(old('stock_market_id') == $value->id) selected @endif>{{ $value->name }}</option>
                @endforeach
            </select>
            @if($errors->first('stock_market_id'))
			<p class="help-block error">{{ $errors->first('stock_market_id') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">提交</button>
        </div>
    </div>
</form>
@endsection