@extends('layouts.main')

@section('title', '公司')

@section('content')
<ol class="breadcrumb">
    @include('company.company.breadcrumb')
    <li class="active">公司列表</li>
  </ol>
<div class="btn-group" role="group">
    <a class="btn btn-primary" href="{{ action('Company\CompanyController@create') }}">添加</a>
</div>

<table class="table table-striped table-bordered table-hover table-condensed">
    <tbody>
        <tr>
            <th>序列号</th>
            <th>公司全称</th>
            <th>简称</th>
            <th>代码</th>
            <th>所属行业</th>
            <th>证券市场</th>
            <th>操作</th>
        </tr>
        @foreach($models as $model)
        <tr>
            <td>{{ $model->id }}</td>
            <td><a href="{{ action('Company\CompanyController@show', $model) }}">{{ $model->name }}</a></td>
            <td>{{ $model->abbreviation }}</td>
            <td>{{ $model->code }}</td>
            <td>{{ $model->industry->name }}</td>
            <td>{{ $model->stock_market->name }}</td>
            <td>
                <a class="btn btn-warning btn-sm" href="{{ action('Company\CompanyController@edit', $model) }}">编辑</a>
                <a class="btn btn-primary btn-sm" href="{{ action('Report\BalanceController@tabular', ['company_code' => $model->code]) }}">资产负债表</a>
                <!-- <button type="button" class="btn btn-danger btn-sm" name="delete" value="{{ $model->id }}">删除</button> -->
            </td>
        </tr>
        @endforeach
    </tbody>
   
</table>

<script>
    $(document).ready(function(){
        $("button[name=delete]").click(function(){
            var that = $(this);
            var id = that.attr("value");
            $.post("/company/company/" + id, {
                '_method': 'DELETE',
                '_token': '{{ csrf_token() }}',
            }, function(data){
                switch(data.status){
                    case 0:
                        that.parent().parent('tr').remove();
                        alert("删除信息成功！");
                        break;
                    case 1:
                        alert(data.info);
                        break;
                }
            });
        });
    });

</script>
@endsection