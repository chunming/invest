@extends('layouts.main')

@section('title', '公司数据')

@section('content')
<div class="row">
    <div class="col-md-2"><a href="{{ action('Company\IndustryController@index') }}" class="btn btn-md btn-primary">行业列表</a></div>
    <div class="col-md-2"><a href="{{ action('Company\CompanyController@index') }}" class="btn btn-md btn-primary">公司列表</a></div>
    <div class="col-md-2"><a href="{{ action('Company\BalanceSheetController@index') }}" class="btn btn-md btn-primary">资产负债表明细</a></div>
</div>
@endsection