@extends('layouts.main')

@section('title', '行业')

@section('content')
<ol class="breadcrumb">
    @include('company.industry.breadcrumb')
    <li class="active">行业列表</li>
  </ol>
<div class="btn-group" role="group">
    <a class="btn btn-primary" href="{{ action('Company\IndustryController@create') }}">添加</a>
</div>

<table class="table table-striped table-bordered table-hover table-condensed">
    <tbody>
        <tr>
            <th>序列号</th>
            <th>名称</th>
            <th>操作</th>
        </tr>
        @foreach($models as $model)
        <tr>
            <td>{{ $model->id }}</td>
            <td>{{ $model->name }}</td>
            <td>
                <a class="btn btn-warning btn-sm" href="{{ action('Company\IndustryController@edit', $model) }}">编辑</a>
                <button type="button" class="btn btn-danger btn-sm" name="delete" value="{{ $model->id }}">删除</button>
            </td>
        </tr>
        @endforeach
    </tbody>
   
</table>

<script>
    $(document).ready(function(){
        $("button[name=delete]").click(function(){
            var that = $(this);
            var id = that.attr("value");
            $.post("/company/industry/" + id, {
                '_method': 'DELETE',
                '_token': '{{ csrf_token() }}',
            }, function(data){
                switch(data.status){
                    case 0:
                        that.parent().parent('tr').remove();
                        alert("删除信息成功！");
                        break;
                    case 1:
                        alert(data.info);
                        break;
                }
            });
        });
    });

</script>
@endsection