@extends('layouts.main')

@section('title', '行业编辑')

@section('content')
<ol class="breadcrumb">
    @include('company.industry.breadcrumb')
    <li class="active">编辑</li>
</ol>
<div class="btn-group" role="group">
    <a class="btn btn-primary" href="{{ action('Company\IndustryController@index') }}">返回</a>
</div>
<form class="form-horizontal" method="post" action="{{ action('Company\IndustryController@update', $model) }}">
    @csrf
    @method('PUT')

    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">名称</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $model->name) }}">
            @if($errors->first('name'))
			<p class="help-block error">{{ $errors->first('name') }}</p>
			@endif
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">提交</button>
        </div>
    </div>
</form>
@endsection