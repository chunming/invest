@extends('layouts.main')

@section('title', '行业显示')

@section('content')
<ol class="breadcrumb">
    @include('company.industry.breadcrumb')
    <li class="active">显示</li>
</ol>
<div class="btn-group" role="group">
    <a class="btn btn-primary" href="{{ action('Company\IndustryController@index') }}">返回</a>
</div>

@include('common.alert')

<form class="form-horizontal">

    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">名称</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="name" name="name" value="{{ $model->name }}" disabled>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <a href="{{ action('Company\IndustryController@index') }}" class="btn btn-primary">返回</a>
            <a href="{{ action('Company\IndustryController@create') }}" class="btn btn-primary">添加</a>
            <a href="{{ action('Company\IndustryController@edit', $model) }}" class="btn btn-warning">编辑</a>
        </div>
    </div>
</form>
@endsection