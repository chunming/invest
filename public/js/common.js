function request_balance_item(company_code, list, parent_id = 0) {
    // alert($parent_id);
    $.get('/financial_basis/balance_item/balance_item_list/' + company_code, function (result) {
        switch (result.status) {
            case 0:
                // var html = '<option value="0">请选择上级项目</option>';
                var html = '';
                for (var i = 0; i < result.data.length; i++) {
                    html += '<option value="' + result.data[i].id + '"' + (result.data[i].id == parent_id ? 'selected' : '') + '>' + result.data[i].name + '</option>';
                }
                list.append(html);
                break;
        }
    });
}

function getUrlParams($key) {
    var url = window.location.search.substring(1);
    if (url == '') {
        return false;
    }

    var paramsArr = url.split('&');
    for (var i = 0; i < paramsArr.length; i++) {
        var combina = paramsArr[i].split("=");
        if (combina[0] == $key) {
            return combina[1];
        }
    }
    return false;
}