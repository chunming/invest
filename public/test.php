<?php
trait Eat
{
    public static $name = 'wangcai';
    public static $count = 0;
    public static function eat()
    {
        self::$count++;
        echo self::$name . ' is eating!';
        echo '<br />';
        echo self::$count;
        echo '<br />';
    }
}


class Dog
{
    use Eat;
}

Dog::eat();
Dog::eat();
Dog::eat();
// wangcai is eating!
// 1
// wangcai is eating!
// 2
// wangcai is eating!
// 3