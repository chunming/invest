## About Report
维护上市公司的基础数据，各种报告。 分析资产负债表、利润表、现金流量表。 通过可视化的图形展示方式，更直接观察数据。

## 模块
- 公司  维护公司的基本信息
- 行业  维护行业信息
- 资产负债表列项目  维护财务上资产负债表列项
- 资产负债表明细    维护资产负债表列项
- 资产负债表    表格形式
- 资产负债表    可视化形式

## Env文件 配置

- QUANTITY_PER_PAGE = 50    # 设置列表页每页显示数量

## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
