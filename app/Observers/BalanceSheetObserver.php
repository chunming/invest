<?php

namespace App\Observers;


use App\Models\Company\BalanceSheet;

class BalanceSheetObserver
{
    /**
     * Handle the balance sheet "created" event.
     *
     * @param  \App\BalanceSheet  $balanceSheet
     * @return void
     */
    public function created(BalanceSheet $balanceSheet)
    {
        //
    }

    /**
     * Handle the balance sheet "updated" event.
     *
     * @param  \App\BalanceSheet  $balanceSheet
     * @return void
     */
    public function updated(BalanceSheet $balanceSheet)
    {
        //
    }

    /**
     * Handle the balance sheet "deleted" event.
     *
     * @param  \App\BalanceSheet  $balanceSheet
     * @return void
     */
    public function deleted(BalanceSheet $balanceSheet)
    {
        //
    }

    /**
     * Handle the balance sheet "restored" event.
     *
     * @param  \App\BalanceSheet  $balanceSheet
     * @return void
     */
    public function restored(BalanceSheet $balanceSheet)
    {
        //
    }

    /**
     * Handle the balance sheet "force deleted" event.
     *
     * @param  \App\BalanceSheet  $balanceSheet
     * @return void
     */
    public function forceDeleted(BalanceSheet $balanceSheet)
    {
        //
    }

    public function saving(BalanceSheet $balanceSheet)
    {
        unset($balanceSheet->multiple);
    }
}
