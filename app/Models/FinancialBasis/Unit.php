<?php

namespace App\Models\FinancialBasis;

use App\Models\Model;

class Unit extends Model
{
    protected $table = 'unit';
}
