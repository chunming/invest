<?php

namespace App\Models\FinancialBasis;

use App\Models\Model;

class FinancialReportRange extends Model
{
    protected $table = 'financial_report_range';
}
