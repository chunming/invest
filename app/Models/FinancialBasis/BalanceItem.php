<?php

namespace App\Models\FinancialBasis;

use App\Models\Company\Company;
use App\Models\Model;
use App\Models\Traits\NoLimit;

class BalanceItem extends Model
{
    use NoLimit;

    protected $table = 'balance_item';

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_code', 'code');
    }

    public function parent()
    {
        return $this->belongsTo(BalanceItem::class, 'parent_id', 'id');
    }

    public function getSort($where = [])
    {
        $data = self::where($where)->orderBy('sort')->get();
        return $this->sortNoLimit($data);
    }
}
