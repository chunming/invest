<?php

namespace App\Models\FinancialBasis;

use App\Models\Model;

class FinancialReportCategory extends Model
{
    protected $table = 'financial_report_category';
}
