<?php

namespace App\Models\Traits;

trait NoLimit
{
    public function sortNoLimit($data, int $start = 0, int $level = 0, string $primary_id_name = 'id', string $parent_id_name = 'parent_id'): array
    {
        static $result = [];
        foreach ($data as $value) {

            // dd($value->$parent_id_name);
            if ($value->$parent_id_name == $start) {
                // dump($value->$primary_id_name);
                $value->level = $level;
                $result[] = $value;
                $this->sortNoLimit($data, $value->$primary_id_name, $level + 1);
            }
        }
        // dump($result);
        return $result;
    }
}
