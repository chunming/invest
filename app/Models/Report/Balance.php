<?php

namespace App\Models\Report;

use App\Models\Company\BalanceSheet;

class Balance
{
    public $company_code;
    public $year;
    public $report_category_id = 4;
    public $report_range_id = 2;

    //数组，键名为资产负债表列项目id
    private $balance_sheets = [];

    public function __construct(string $company_code, int $year, int $report_category_id, int $report_range_id)
    {
        $this->company_code = $company_code;
        $this->year = $year;
        $this->report_category_id = $report_category_id;
        $this->report_range_id = $report_range_id;
    }

    public function getItem(int $balance_item_id): float
    {
        $result = 0.00;
        if (array_key_exists($balance_item_id, $this->balance_sheets)) {
            $result = $this->balance_sheets[$balance_item_id]->amount;
        }
        return $result;
    }

    //添加资产负债表明细
    public function addSheet(BalanceSheet $balance_sheet)
    {
        if (!array_key_exists($balance_sheet->balance_item_id, $this->balance_sheets)) {
            $this->balance_sheets[$balance_sheet->balance_item_id] = $balance_sheet;
        }
    }
}
