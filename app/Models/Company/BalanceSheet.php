<?php

namespace App\Models\Company;

use App\Models\FinancialBasis\BalanceItem;
use App\Models\FinancialBasis\FinancialReportCategory;
use App\Models\FinancialBasis\FinancialReportRange;
use App\Models\Model;

class BalanceSheet extends Model
{
    protected $table = 'balance_sheet';

    protected $guarded = ['multiple'];

    public function setAmountAttribute($value)
    {
        $value = str_replace(',', '', $value);
        $value = $value * $this->multiple;
        $this->attributes['amount'] = $value;
    }


    public function company()
    {
        return $this->belongsTo(Company::class, 'company_code', 'code');
    }

    public function balance_item()
    {
        return $this->belongsTo(BalanceItem::class, 'balance_item_id', 'id');
    }

    public function financial_report_category()
    {
        return $this->belongsTo(FinancialReportCategory::class, 'report_category_id', 'id');
    }

    public function financial_report_range()
    {
        return $this->belongsTo(FinancialReportRange::class, 'report_range_id', 'id');
    }
}
