<?php

namespace App\Models\Company;

use App\Models\Model;

class Company extends Model
{
    protected $table = 'company';

    public function industry()
    {
        return $this->belongsTo(Industry::class, 'industry_id', 'id');
    }

    public function stock_market()
    {
        return $this->belongsTo(StockMarket::class, 'stock_market_id', 'id');
    }
}
