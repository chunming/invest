<?php

namespace App\Http\Requests\FinancialBasis;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BalanceItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'company_code' => 'exists:company,code',
            'name' => 'required|max:100',
            'sort' => 'required|integer|min:0',
            'display_bold' => Rule::in(['1', '2']),
        ];
        switch (request()->method) {
            case 'POST':
                break;
            case 'PUT':
                break;
            default:
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'company_code.exists' => '请选择公司！',
            'name.required' => '请输入列项名称！',
            'name.max' => '列项名称长度最大为100个字符！',
            'sort.required' => '请输入排序数字！',
            'sort.integer' => '排序数字为正整数！',
            'sort.min' => '排序数字为正整数！',
            'display_bold.in' => '显示是否加粗数据异常！',
        ];
    }
}
