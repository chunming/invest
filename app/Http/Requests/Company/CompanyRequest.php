<?php

namespace App\Http\Requests\Company;

use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'industry_id' => 'exists:industry,id',
            'stock_market_id' => 'exists:stock_market,id',
        ];
        switch (request()->method) {
            case 'POST':
                $rules['name'] = 'required|max:100|unique:company,name';
                $rules['abbreviation'] = 'required|max:20|unique:company,abbreviation';
                $rules['code'] = 'required|max:10|unique:company,code';
                break;
            case 'PUT':
                $rules['name'] = 'required|max:100|unique:company,name,' . request()->company;
                $rules['abbreviation'] = 'required|max:20|unique:company,abbreviation,' . request()->company;
                $rules['code'] = 'required|max:10|unique:company,code,' . request()->company;
                break;
            default:
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => '请输入公司全称！',
            'name.max' => '公司全称长度最大为100个字符！',
            'name.unique' => '公司全称已经存在！',
            'abbreviation.required' => '请输入公司简称！',
            'abbreviation.max' => '公司简称长度最大为20个字符！',
            'abbreviation.unique' => '公司简称已经存在！',
            'code.required' => '请输入股票代码！',
            'code.max' => '股票代码长度最大为10个字符！',
            'code.unique' => '股票代码已经存在！',
            'industry_id.exists' => '请选择行业！',
            'stock_market_id.exists' => '请选择证券市场！',
        ];
    }
}
