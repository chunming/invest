<?php

namespace App\Http\Requests\Company;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BalanceSheetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'company_code' => 'exists:company,code',
            'year' => 'integer|digits:4',
            'report_category_id' => 'exists:financial_report_category,id',
            'report_range_id' => 'exists:financial_report_range,id',
            'balance_item_id' => 'exists:balance_item,id',
            'amount' => 'required',
        ];
        switch (request()->method) {
            case 'POST':
                break;
            case 'PUT':
                break;
            default:
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'company_code.exists' => '请选择公司！',
            'year.integer' => '请输入正确的年份！',
            'year.digits' => '请输入正确的年份！',
            'report_category_id.exists' => '请选择财报类别！',
            'report_range_id.exists' => '请选择财报范围！',
            'balance_item_id.exists' => '请选择资产负债表列项！',
            'amount.required' => '请输入金额（单位元）！',
            // 'amount.numeric' => '金额为数字格式！',
        ];
    }
}
