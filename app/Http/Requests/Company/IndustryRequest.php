<?php

namespace App\Http\Requests\Company;

use Illuminate\Foundation\Http\FormRequest;

class IndustryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        switch (request()->method) {
            case 'POST':
                $rules['name'] = 'required|max:20|unique:industry,name';
                break;
            case 'PUT':
                $rules['name'] = 'required|max:20|unique:industry,name,' . request()->industry;
                break;
            default:
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => '请输入行业名称！',
            'name.max' => '行业名称长度最大为20个字符！',
            'name.unique' => '行业名称已经存在！',
        ];
    }
}
