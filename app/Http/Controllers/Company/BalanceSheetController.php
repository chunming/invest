<?php

namespace App\Http\Controllers\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Company\BalanceSheetRequest;
use App\Models\Company\BalanceSheet;
use App\Models\Company\Company;
use App\Models\FinancialBasis\BalanceItem;
use App\Models\FinancialBasis\FinancialReportCategory;
use App\Models\FinancialBasis\FinancialReportRange;
use App\Models\FinancialBasis\Unit;

class BalanceSheetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $financial_report_range = FinancialReportRange::all();
        $financial_report_category = FinancialReportCategory::all();
        $where = [];
        ($request->company_code && $request->company_code != 0) ? $where['company_code'] = $request->company_code : '';
        $request->year ? $where['year'] = $request->year : '';
        is_numeric($request->financial_report_category) ? $where['report_category_id'] = $request->financial_report_category : '';
        is_numeric($request->financial_report_range) ? $where['report_range_id'] = $request->financial_report_range : '';
        $companies = Company::all();
        $models = BalanceSheet::where($where)->paginate(config('basis.quantity_per_page'));
        return view('company.balance_sheet.index', [
            'models' => $models,
            'companies' => $companies,
            'financial_report_range' => $financial_report_range,
            'financial_report_category' => $financial_report_category,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $companies = Company::all();
        $units = Unit::all();
        // $balance_items = (new BalanceItem())->getSort(['company_code' => $request->company_code]);
        $financial_report_range = FinancialReportRange::all();
        $financial_report_category = FinancialReportCategory::all();
        return view('company.balance_sheet.create', [
            'companies' => $companies,
            // 'balance_items' => $balance_items,
            'financial_report_range' => $financial_report_range,
            'financial_report_category' => $financial_report_category,
            'units' => $units,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BalanceSheetRequest $request)
    {
        //判断是否有相同的资产负债表明细存在。
        $model = BalanceSheet::where([
            'company_code' => $request->company_code,
            'year' => $request->year,
            'report_category_id' => $request->report_category_id,
            'report_range_id' => $request->report_range_id,
            'balance_item_id' => $request->balance_item_id,
        ])->first();
        if ($model) {
            return redirect()->back()->with('fail', '存在相同的资产负债明细！');
        }

        $model = new BalanceSheet();
        $model->company_code = $request->company_code;
        $model->year = $request->year;
        $model->report_category_id = $request->report_category_id;
        $model->report_range_id = $request->report_range_id;
        $model->balance_item_id = $request->balance_item_id;
        $model->multiple = $request->multiple;
        $model->amount = $request->amount;
        $cookie = \Cookie('multiple', $request->multiple);
        $model->save();
        return redirect()->action('Company\BalanceSheetController@show', $model)->with('success', '添加成功！')->withCookie($cookie);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $model = BalanceSheet::find($id);
        return view('company.balance_sheet.show', ['model' => $model]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $units = Unit::all();
        $model = BalanceSheet::find($id);
        $companies = Company::all();
        $financial_report_range = FinancialReportRange::all();
        $financial_report_category = FinancialReportCategory::all();
        $balance_items = (new BalanceItem())->getSort();
        return view('company.balance_sheet.edit', [
            'companies' => $companies,
            'balance_items' => $balance_items,
            'model' => $model,
            'financial_report_range' => $financial_report_range,
            'financial_report_category' => $financial_report_category,
            'units' => $units,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BalanceSheetRequest $request, $id)
    {
        $model = BalanceSheet::find($id);
        $model->company_code = $request->company_code;
        $model->year = $request->year;
        $model->report_category_id = $request->report_category_id;
        $model->report_range_id = $request->report_range_id;
        $model->balance_item_id = $request->balance_item_id;
        $model->multiple = $request->multiple;
        $model->amount = $request->amount;
        $cookie = \Cookie('multiple', $request->multiple);
        $model->save();
        return redirect()->action('Company\BalanceSheetController@show', $model)->with('success', '编辑成功！')->withCookie($cookie);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        BalanceSheet::find($id)->delete();
        return ['status' => 0];
    }
}
