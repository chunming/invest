<?php

namespace App\Http\Controllers\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Company\CompanyRequest;
use App\Models\Company\Company;
use App\Models\Company\Industry;
use App\Models\Company\StockMarket;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = Company::paginate(config('basis.quantity_per_page'));
        return view('company.company.index', ['models' => $models]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $industries = Industry::all();
        $stock_markets = StockMarket::all();
        return view('company.company.create', [
            'industries' => $industries,
            'stock_markets' => $stock_markets
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request)
    {
        $model = new Company();
        $model->name = $request->name;
        $model->abbreviation = $request->abbreviation;
        $model->code = $request->code;
        $model->industry_id = $request->industry_id;
        $model->stock_market_id = $request->stock_market_id;
        $model->save();
        return redirect()->action('Company\CompanyController@show', $model)->with('success', '添加成功！');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Company::find($id);
        return view('company.company.show', ['model' => $model]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Company::find($id);
        $industries = Industry::all();
        $stock_markets = StockMarket::all();
        return view('company.company.edit', [
            'industries' => $industries,
            'stock_markets' => $stock_markets,
            'model' => $model
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyRequest $request, $id)
    {
        $model = Company::find($id);
        $model->name = $request->name;
        $model->abbreviation = $request->abbreviation;
        $model->code = $request->code;
        $model->industry_id = $request->industry_id;
        $model->stock_market_id = $request->stock_market_id;
        $model->save();
        return redirect()->action('Company\CompanyController@show', $model)->with('success', '编辑成功！');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //判断是否存在子项
        // $model = Company::where(['parent_id' => $id])->first();
        // if ($model) {
        //     return ['status' => 1, 'info' => '存在子项，不能删除！'];
        // }
        Company::find($id)->delete();
        return ['status' => 0];
    }
}
