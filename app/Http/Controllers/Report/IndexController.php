<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index()
    {
        return view('report.index.index');
    }
}
