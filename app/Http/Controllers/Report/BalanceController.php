<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Company\Company;
use App\Models\FinancialBasis\FinancialReportCategory;
use App\Models\FinancialBasis\FinancialReportRange;
use App\Models\Report\BalanceList;

class BalanceController extends Controller
{
    public function tabular(Request $request)
    {
        $companies = Company::all();
        $financial_report_category = FinancialReportCategory::all();
        $financial_report_range = FinancialReportRange::all();


        $company_code = $request->company_code ? $request->company_code : '000002';
        $year_end = $request->year_end ? $request->year_end : date('Y') - 1;
        $year_start = $request->year_start ? $request->year_start : $year_end - 5;
        $report_category_id = $request->report_category_id ? $request->report_category_id : 4;
        $report_range_id = $request->report_range_id ? $request->report_range_id : 1;

        $balance_list = new BalanceList($company_code, ['start' => $year_start, 'end' => $year_end], $report_range_id, $report_category_id);
        return view('report.balance.tabular', [
            'balance_list' => $balance_list,
            'year_start' => $year_start,
            'year_end' => $year_end,
            'companies' => $companies,
            'financial_report_category' => $financial_report_category,
            'financial_report_range' => $financial_report_range
        ]);
    }

    public function visual()
    {
    }
}
