<?php

namespace App\Http\Controllers\FinancialBasis;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\FinancialBasis\BalanceItemRequest;
use App\Models\Company\Company;
use App\Models\FinancialBasis\BalanceItem;

class BalanceItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $companies = Company::all();
        $company_code = $request->company_code ? $request->company_code : 0;
        $models = (new BalanceItem())->getSort(['company_code' => $company_code]);
        return view('financial_basis.balance_item.index', [
            'models' => $models,
            'companies' => $companies
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::all();
        // $balance_items = (new BalanceItem())->getSort();
        return view('financial_basis.balance_item.create', [
            // 'balance_items' => $balance_items,
            'companies' => $companies,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BalanceItemRequest $request)
    {
        //判断是否存在相同的列项
        //公司、列项名称、父级
        $model = BalanceItem::where([
            'company_code' => $request->company_code,
            'name' => $request->name,
            'parent_id' => $request->parent_id,
        ])->first();

        if ($model) {
            return redirect()->back()->with('fail', '已经存在相同的资产负债列项！');
        }

        $model = new BalanceItem();
        $model->company_code = $request->company_code;
        $model->name = $request->name;
        $model->parent_id = $request->parent_id;
        $model->sort = $request->sort;
        $model->display_bold = $request->display_bold;
        $model->save();
        return redirect()->action('FinancialBasis\BalanceItemController@show', $model)->with('success', '添加成功！');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = BalanceItem::find($id);
        return view('financial_basis.balance_item.show', ['model' => $model]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $companies = Company::all();
        $model = BalanceItem::find($id);
        $balance_items = (new BalanceItem())->getSort(['company_code' => $model->company_code]);
        return view('financial_basis.balance_item.edit', [
            'model' => $model,
            'balance_items' => $balance_items,
            'companies' => $companies,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BalanceItemRequest $request, $id)
    {
        $model = BalanceItem::find($id);
        $model->company_code = $request->company_code;
        $model->name = $request->name;
        $model->parent_id = $request->parent_id;
        $model->sort = $request->sort;
        $model->display_bold = $request->display_bold;
        $model->save();
        return redirect()->action('FinancialBasis\BalanceItemController@show', $model)->with('success', '编辑成功！');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //判断是否存在子项
        $model = BalanceItem::where(['parent_id' => $id])->first();
        if ($model) {
            return ['status' => 1, 'info' => '存在子项，不能删除！'];
        }

        BalanceItem::find($id)->delete();
        return ['status' => 0];
    }


    public function balanceItemList(Request $request)
    {
        $company_code = $request->company_code;
        $balance_items = (new BalanceItem())->getSort(['company_code' => $company_code]);
        return ['status' => 0, 'data' => $balance_items];
    }
}
