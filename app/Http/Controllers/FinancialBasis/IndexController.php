<?php

namespace App\Http\Controllers\FinancialBasis;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index()
    {
        return view('financial_basis.index.index');
    }
}
