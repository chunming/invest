<?php

namespace App\Providers;

use App\Models\Company\BalanceSheet;
use App\Observers\BalanceSheetObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        BalanceSheet::observe(BalanceSheetObserver::class);
    }
}
