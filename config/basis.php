<?php
return [
    //显示是否加粗
    'display_bold' => [
        1 => '否',
        2 => '是'
    ],

    //分页数量
    'quantity_per_page' => env('QUANTITY_PER_PAGE', 50),

];
