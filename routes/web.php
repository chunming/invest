<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

//财务基础
Route::get('/financial_basis', 'FinancialBasis\IndexController@index');

// balanceItemList
Route::get('/financial_basis/balance_item/balance_item_list/{company_code}', 'FinancialBasis\BalanceItemController@balanceItemList');
Route::resource('/financial_basis/balance_item', 'FinancialBasis\BalanceItemController');


//公司数据
Route::get('/company/index', 'Company\IndexController@index');
Route::resource('/company/industry', 'Company\IndustryController');
Route::resource('/company/company', 'Company\CompanyController');
Route::resource('/company/balance_sheet', 'Company\BalanceSheetController');


//报表
Route::get('/report/index', 'Report\IndexController@index');
Route::get('/report/balance_tabular', 'Report\BalanceController@tabular');
Route::get('/report/balance_visual', 'Report\BalanceController@visual');
